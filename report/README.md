# README #

Repository for hosting personal LaTeX templates.

This template uses external fonts, such as [TeX Gyre Heros](http://www.gust.org.pl/projects/e-foundry/tex-gyre/heros).

이 템플릿은 [한글출판인회의](http://www.kopus.org/Default.aspx)의 KoPub바탕체와 KoPub돋음체를 사용합니다.
