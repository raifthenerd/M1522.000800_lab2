%%% MAIN FILE
\documentclass[article,oneside,a4paper,wordlikemargin,]{xmbk}

\lstset{language=C}

% metadata
\title{System Programming: Kernel Driver Lab}
\author{2012-11853 Seokjin Han}
\date{October 14, 2015}

% document: start writing!
\begin{document}
\maketitle
\section{Precedent Research}
\subsection{Process \citep{textbook}}
A process is usually defined as an instance of a program in execution. The
process provides an independent logical control flow and a private address
space, which are key abstractions for tricking our program into having
exclusive use of the processor and the memory.
\subsubsection{Linux APIs Corresponding to Process}
\paragraph{\texttt{task\_struct}}
The kernel stores the list of processes in the task list, and each element in
the task list has a type \code{struct task_struct}. \code{task_struct} contains
all the information about a specific process.
\begin{lstlisting}%
[caption=(Parts of) Linux data structure for describing a process]
#include <linux/sched.h>
#define TASK_COMM_LEN 16

struct task_struct {
  pid_t pid;
  struct task_struct __rcu *parent;
  char comm[TASK_COMM_LEN];
  ...
};
\end{lstlisting}
\paragraph{\texttt{fork}} Calling the \code{fork} function creates a new
running child process from the parent process. The \code{fork} copies the
virtual address space and the file descriptors from parent to the child, so
they seem almost identical. Only difference is their PID; child process will
always have a larger PID than its parent process.
\begin{lstlisting}%
[caption=Linux functions for creating a child process]
#include <sys/types.h>
#include <unistd.h>

pid_t fork(void);
\end{lstlisting}
\paragraph{\texttt{execve}} \code{execve} functions load and run a new program
from executables and replace current process with it by overwritting text,
data, bss, stack, etc. \code{execve} does not return on success.
\begin{lstlisting}%
[caption=Linux functions for executing a file]
#include <unistd.h>

int execve(const char *file, char *const argv[], char *const envp[]);
\end{lstlisting}
\paragraph{\texttt{wait}} Calling \code{wait} or \code{waitpid} makes a process
waiting for its children to terminate or stop. \code{wait(NULL)} is equivalent
with \code{waitpid(-1, NULL, 0)}.
\begin{lstlisting}%
[caption=Linux functions for waiting for process to change state]
#include <sys/types.h>
#include <sys/wait.h>

pid_t wait(int *status);
pid_t waitpid(pid_t pid, int *status, int options);
\end{lstlisting}
\subsection{Kernel Module \citep{device_drivers}}
A kernel module is an object file that can be loaded and unloaded dynamically
into the kernel at runtime with \code{insmod} and \code{rmmod} commands. The
Linux kernel supports several types of modules, including device drivers. In
this lab, a character device will be implemented, which can be accessed as a
stream of bytes. Char device drivers usually implements open, close, read and
write system calls. Char devices are very similar to regular files, but they
can be only accessed sequentially while we can move freely in the regular
files.

Kernel modules and applications have several differences. One of the notable
difference is the difference in memory space; a module runs in kernel space,
while applications run in user space. To protect against unauthorized access to
resources, the CPU enforces difference mode between modules and applications.
In Linux, the kernel executes in kernel mode and everything is allowed, while
the application executes in user mode with lots of regulations to hardware and
memory. Also, the CPU enforces different memory mappings between the kernel and
the user.
\subsubsection{Linux APIs Corresponding to Kernel Module}
\paragraph{\texttt{copy\_to\_user}}
Since the kernel space and the user space are strictly separated, a special
kernel function is used to transfer data from or to user
space. \code{copy_to_user} function, as the name suggests, copies a data from
kernel space to user space. Sometimes \code{access_ok} function is used to
verify whether the given address of user space is readable/writable.
\begin{lstlisting}%
[caption=Linux function for copy data from kernel space to user space]
#include <asm/uaccess.h>

unsigned long copy_to_user (void __user *to, const void *from,
                            unsigned long count);
int access_ok(int type, const void *addr, unsigned long size);

\end{lstlisting}
\paragraph{\texttt{ioctl}}
The device driver has its own driver-specific operations, such as hardware
controls. In Linux, these operations are usually supported via the
\code{ioctl} so the operations can be requested in user mode via a simple data
transfer. In this lab, the ioctl is used in the performance monitoring unit.
\begin{lstlisting}%
[caption=Linux functions for call \code{ioctl} in user mode]
#include <sys/ioctl.h>

int ioctl(int fd, unsigned long request, ...);
\end{lstlisting}
\subsection{Model Specific Register of Intel CPU \citep{intel_man}}
A model specific register (MSR) is introduced by CPU manufacturers, which is a
special control registers used for debugging, performance monitoring or other
purposes.

Since the MSR is a platform-dependent feature, we assume to use a modern
Intel single-core CPU in this lab. Intel provide two machine instructions to
access MSRs: \code{rdmsr} for reading, \code{wrmsr} for writing. In the
following section, we only consider few MSRs that used to obtain our goal;
performance monitoring.

To monitor the performance event, first we have to configure the performance
event by writing bit fields of MSRs (\code{IA32_PERFEVTSELx}). Then, the result
of the event is reported in the corresponding counter MSRs (\code{IA32_PMCx}).
Every performance events are enabled or disabled according to the bit field in
the MSR \code{IA32_PERF_GLOBAL_CTRL} at address \code{0x38F}.

\section{Implementation}
\subsection{Process Tree}
First, define \code{PtreeInfo} as \Cref{code:ptreeinfo}.
\begin{lstlisting}%
[label=code:ptreeinfo,caption=Definition of \code{PtreeInfo}.]
struct PtreeInfo {
    int pid;
    char comm[16];
    union {
        int pid_parent;
        struct PtreeInfo* p_parent;
    };
};
\end{lstlisting}
The field \code{pid} and \code{comm} is for process pid and process name
respectfully. To describe inforation about its parent, \code{PtreeInfo} has a
union of \code{pid_parent} and \code{p_parent}, which is explained below.

The \code{getPtree} and \code{get_ptree} function defined as
\Cref{code:getPtree} and \Cref{code:get_ptree} respectfully.
\begin{lstlisting}%
[label=code:getPtree,caption=Implementation of \code{getPtree}.]
int getPtree(int fd, int pid) {
    int tmp;
    struct PtreeInfo* p_temp = &ptree;
    do {
        p_temp->pid = pid;
        tmp = ioctl(fd, IOCTL_GET_PTREE, p_temp);
        if (tmp < 0) {
            return tmp;
        }
        pid = p_temp->pid_parent;
        if (pid < 1) {
            p_temp->p_parent = NULL;
            return 0;
        }
        p_temp->p_parent = malloc(sizeof(struct PtreeInfo));
        p_temp = p_temp->p_parent;
    } while (1);
}
\end{lstlisting}
\begin{lstlisting}%
[label=code:get_ptree,caption=Implementation of \code{get_ptree}.]
int get_ptree(unsigned long ioctl_param) {
    struct PtreeInfo* ptree = (struct PtreeInfo*) ioctl_param;
    struct task_struct* ptask = pid_task(find_vpid(ptree->pid), PIDTYPE_PID);
    ptree->pid = ptask->pid;
    if (access_ok(VERIFY_WRITE, ptree->comm, 16*sizeof(char)) == 0||
        copy_to_user(ptree->comm, ptask->comm, 16*sizeof(char)) > 0) {
        return -EFAULT;
    }
    if (ptask->parent != NULL) {
        ptree->pid_parent = ptask->parent->pid;
    } else {
        ptree->pid_parent = 0;
    }
    return 0;
}
\end{lstlisting}
\code{get_ptree} reads pid from given ioctl parameter, and writes back the
corresponding process name and parent's pid to the parameter. In user space,
\code{getPtree} repeats allocating a new node and calling \code{get_ptree}
until the fetched process node tells that it has no parent process.

Why we are doing this? To create a tree data structure, dynamic memory
allocation is necessary. However, since the kernel space and user space are
separated, we cannot return a complete process tree by single call of
\code{getPtree} from kernel to user. Instead, we transfer the single node to
avoid the problem; dynamic allocation in user space and fill the contents in
kernel mode.
\subsection{Performance Monitoring Unit}
Since we are only interested in three performance events -- instruction
retired, stalled cycles, and core cycles -- only three MSRs have to be
enabled. These events are corresponding to \code{UOPS_ISSUED.ANY},
\code{UOPS_ISSUED.STALL_CYCLES}, and \code{CPL_CYCLES.RING123}, respectfully.
Therefore, \code{msr_set_start} have to expressed as \Cref{code:msr_set_start}.
\begin{lstlisting}%
[label=code:msr_set_start,caption=Structure of \code{msr_set_start}.]
struct MsrInOut msr_set_start[] = {
    { MSR_WRITE, PERF_EVT_SEL0,
      PERF_EVT_SEL_USR | PERF_EVT_SEL_EN | INST_RETIRED,
      0x00 },
    { MSR_WRITE, PERF_EVT_SEL1,
      PERF_EVT_SEL_USR | PERF_EVT_SEL_EN | RESOURCE_STALLS,
      0x00 },
    { MSR_WRITE, PERF_EVT_SEL2,
      PERF_EVT_SEL_USR | PERF_EVT_SEL_EN | CPU_CLK_UNHALTED,
      0x00 },
    { MSR_WRITE, PERF_GLOBAL_CTRL,
      PERF_GLOBAL_CTRL_EN_PMC0 | PERF_GLOBAL_CTRL_EN_PMC1 |
      PERF_GLOBAL_CTRL_EN_PMC2 },
    { MSR_STOP, 0x00, 0x00 }
};
\end{lstlisting}

In conclusion, we can obtain process tree and performance monitoring results
by \Cref{code:main}.
\begin{lstlisting}%
[label=code:main,caption=Part of the \code{main} function]
if ((pid = fork()) > 0) {
    getPtree(fd, pid);
    print_ptree();
    waitpid(-1, NULL, 0);
    ioctl(fd, IOCTL_MSR_CMDS, msr_stop_read);
    print_profiling(msr_stop_read[2].value,
                    msr_stop_read[3].value,
                    msr_stop_read[4].value);
} else if (pid == 0) {
    getProfiling(fd);
    execv(argv[1], &argv[1]);
} else {
    printf("Cannot fork\n");
    return 1;
}
\end{lstlisting}
\section{Discussion}
\subsection{Profiling Results}
The profiling results are \Cref{result:clever} and
\Cref{result:stupid}. Running on Linux Mint 64-bit (Linux 3.16.0) VMWare guest,
with Arch Linux 64-bit (Linux 4.2.3) host. As the results suggest,
\code{clever} works better than \code{stupid}.
\begin{lstlisting}%
[label=result:clever,caption=Profiling result of \code{clever}.]
-----------------------------------------------------------
TARGET : clever

- Process Tree Information

init
+mdm
 +mdm
  +init
   +xfce4-panel
    +xfce4-terminal
     +bash
      +profiler
       +profiler

- Process Mornitoring Information

inst retired :            16085117154
stalled cycles :            863700375
core cycles :              5095612807

stall rate :                16.949882 %
throughput :                 3.156660 inst/cycles
-----------------------------------------------------------
\end{lstlisting}
\begin{lstlisting}%
[label=result:stupid,caption=Profiling result of \code{stupid}.]
-----------------------------------------------------------
TARGET : stupid

- Process Tree Information

init
+mdm
 +mdm
  +init
   +xfce4-panel
    +xfce4-terminal
     +bash
      +profiler
       +profiler

- Process Mornitoring Information

inst retired :            16053021031
stalled cycles :          26270089374
core cycles :             30491334693

stall rate :                86.155918 %
throughput :                 0.526478 inst/cycles
-----------------------------------------------------------
\end{lstlisting}
\subsection{Performance Monitoring with Architectural Events \citep{intel_man}}
Starting with Intel Core Solo, Intel provides two classes of performance
monitoring capabilities: architectural performance events and non-architectural
performance events. Our profiling results are based on the non-architectural
events, which are not guaranteed to work consistent and might cause malfunctions
on other machines. So, we can consider using architectural events for
portability.

There are seven pre-defined architectural performance events including unhalted
core cycles and instruction retired, which behave consistently across Intel
processors. In addition, there are three MSRs (\code{IA32_FIXED_CTRx}) that
monitor fixed architectural events respectfully. Configuring the fixed-function
PMCs can be done by writing to bit fields in the MSR
(\code{IA32_FIXED_CTR_CTRL}) at address \code{0x38D}. Each
\code{IA32_FIXED_CTRx} counts instruction retired, unhalted core cycles and
unhalted reference cycles.

Thus, we can use FFC0 and FFC1 instead of PMCs that observing
\code{UOPS_ISSUED.ANY} and \code{CPL_CYCLES.RING123}. Or, we can replace
\code{0x010E} to \code{0x00C0}, and/or \code{0x025C} to \code{0x003C}.
\printbibliography
\end{document}
